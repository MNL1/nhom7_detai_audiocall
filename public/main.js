let Peer = window.Peer;
let messagesEl = document.querySelector('.messages');
let peerIdEl = document.querySelector('#connect-to-peer');
let videoEl = document.querySelector('.remote-video');

let logMessage = (message) => {
  let newMessage = document.createElement('div');
  newMessage.innerText = message;
  messagesEl.appendChild(newMessage);
};

let renderVideo = (stream) => {
  videoEl.srcObject = stream;
};

let peer = new Peer({
  host: '/',
  path: '/peerjs/myapp'
});
peer.on('open', (id) => {
  logMessage('My ID: ' + id);
});
peer.on('error', (error) => {
  console.error(error);
});

peer.on('connection', (conn) => {
  logMessage('incoming peer connection!');
  conn.on('data', (data) => {
    logMessage(`received: ${data}`);
  });
  conn.on('open', () => {
    conn.send('Hello!');
  });
});

peer.on('call', (call) => {
  navigator.mediaDevices.getUserMedia({video: false, audio: true})
    .then((stream) => {
      call.answer(stream);
      call.on('stream', renderVideo);
    })
    .catch((err) => {
      console.error('ERROR', err);
    });
});

let connectToPeer = () => {
  let peerId = peerIdEl.value;
  logMessage(`Connecting to ${peerId}...`);
  
  let conn = peer.connect(peerId);
  conn.on('data', (data) => {
    logMessage(`Received: ${data}`);
  });
  conn.on('open', () => {
    conn.send('Hi!');
  });
  
  navigator.mediaDevices.getUserMedia({video: false, audio: true})
    .then((stream) => {
      let call = peer.call(peerId, stream);
      call.on('stream', renderVideo);
    })
    .catch((err) => {
      logMessage('ERROR', err);
    });
};

window.connectToPeer = connectToPeer;